package com.example.deloittetest.Activities;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.deloittetest.Application.Caches.WishlistDataCache;
import com.example.deloittetest.Application.Main;
import com.example.deloittetest.Application.Models.Cart;
import com.example.deloittetest.Application.Models.Product;
import com.example.deloittetest.Application.Utils.Constants;
import com.example.deloittetest.Fragments.ProductListFragment;
import com.example.deloittetest.R;
import com.example.deloittetest.Utils.InterfaceHandlers;
import com.example.deloittetest.ViewModel.CartViewModel;
import com.example.deloittetest.ViewModel.ProductViewModel;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

// BaseActivity contains the common layout for all the pages.
// The sub screens will be different fragments in the BaseActivity
public class BaseActivity extends AppCompatActivity implements InterfaceHandlers.ICartListenerToView, View.OnClickListener,
        InterfaceHandlers.IProductList {

    private IOnWishlistClickListener mWishListClickListener;
    private boolean mIsWishListOpen;
    private List<Cart> mCartList;
    private boolean mCartLayoutClicked;

    public interface IOnWishlistClickListener {
        void onWishListClicked(boolean isClicked);
        void onCartLayoutClicked();
    }

    private static final String PRODUCT_FRAGMENT = "productfragment";
    TextView mCartTotalTv;
    TextView mCartTotalValueTv;
    TextView mShopTitle;
    ImageView mWishListIv;
    RelativeLayout mCartLayout;
    CartViewModel mCartViewModel;
    ProductViewModel mProductViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Main.getInstance().startApplications();
        mCartViewModel = new CartViewModel(this);
        mCartViewModel.setActivityListener(this);
        mCartViewModel.FetchCartItems();

        mProductViewModel = new ProductViewModel();
        mProductViewModel.setActivityListener(this);

        mCartTotalTv = findViewById(R.id.cart_total_tv);
        mCartTotalValueTv = findViewById(R.id.cart_total_value_tv);
        mWishListIv = findViewById(R.id.wish_list_img);
        mCartLayout = findViewById(R.id.cartLayout);
        mShopTitle = findViewById(R.id.shop_title);
        mWishListIv.setOnClickListener(this);
        mCartLayout.setOnClickListener(this);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.product_list_container, ProductListFragment.newInstance())
                .addToBackStack(PRODUCT_FRAGMENT)
                .commitAllowingStateLoss();

        // Reading the wishList elements in the beginning of application
        // and storing in data cache.
        SharedPreferences sharedPreferences
                = getSharedPreferences("MySharedPref",
                MODE_PRIVATE);
        String value = sharedPreferences.getString(Constants.PRICE_KEY, "");

        // Using a set as wishlist does not need to have duplicate items.

        if (!"".equalsIgnoreCase(value)) {
            String[] arrValue = value.split("\\|");
            Set<String> set = new HashSet(Arrays.asList(arrValue));
            WishlistDataCache.getInstance().setDataCache(set);
        }
    }

    @Override
    public void onBackPressed() {

        mShopTitle.setText(R.string.shop_title);
        if (mIsWishListOpen) {
            // If wishlist is opened and back is pressed
            mWishListClickListener.onWishListClicked(false);
            mIsWishListOpen = false;
        } else if (mCartLayoutClicked) {
            // If cart is opened and back is pressed
            // The operation is same as coming back from wishlist, i.e. loading back the products in the adapter.
            mWishListClickListener.onWishListClicked(false);
            mCartLayoutClicked = false;
        } else {
            finish();
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mProductViewModel.setActivityListener(null);

        // Saving the wishList items in SharedPreference so the items remains persistent even when app restarts
        SharedPreferences sharedPreferences
                = getSharedPreferences("MySharedPref",
                MODE_PRIVATE);
        String value = sharedPreferences.getString(Constants.PRICE_KEY, "");
        SharedPreferences.Editor myEdit
                = sharedPreferences.edit();

        String string = String.join("|", WishlistDataCache.getInstance().getProductCache());

        myEdit.putString(Constants.PRICE_KEY, string);
        myEdit.commit();
    }

    @Override
    public void OnProductAdded(String message) {
        // Not required

    }

    @Override
    public void OnCartLoadedInApplication(List<Cart> cartList) {
        Log.i("saz", "Cart size: " + cartList.size());
        mCartTotalTv.setText(getString(R.string.cart_total, cartList.size() + ""));

        mCartList = cartList;
        updateCartTotal();
    }

    private void updateCartTotal() {
        int sum = 0;
        // Calculating cart total
        if (mCartList == null) {
            return;
        }
        for (Cart cart : mCartList) {
            Product prod = mProductViewModel.getProductByID(cart.getProductId());
            if (prod != null) {
                sum += Double.parseDouble(prod.getPrice());
            }
        }

        Log.i("saz", "Cart total value: " + sum);
        if (sum > 0) {
            mCartTotalValueTv.setVisibility(View.VISIBLE);
            mCartTotalValueTv.setText(getString(R.string.total_cart_value, sum + ""));
        } else {
            mCartTotalValueTv.setVisibility(View.GONE);
        }
    }

    @Override
    public void OnCartItemDeleted(int statusCode) {

    }

    public void setWshlistClickListener(IOnWishlistClickListener listener) {
        mWishListClickListener = listener;
    }


    @Override
    public void OnProductsLoaded(List<Product> products, String message) {
        updateCartTotal();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == mWishListIv.getId()) {
            mShopTitle.setText(R.string.str_wishlist);
            mWishListClickListener.onWishListClicked(true);
            mIsWishListOpen = true;
        } else if (view.getId() == mCartLayout.getId()) {
            mShopTitle.setText(R.string.str_cart);
            mCartLayoutClicked = true;
            mWishListClickListener.onCartLayoutClicked();
        }
    }


    // Methods to access viewModel objects in fragments
    public CartViewModel getCartViewModel() {
        return mCartViewModel;
    }

    public ProductViewModel getProductViewModel() {
        return mProductViewModel;
    }
}
