package com.example.deloittetest.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.deloittetest.Application.Caches.WishlistDataCache;
import com.example.deloittetest.Application.Models.Cart;
import com.example.deloittetest.Application.Models.Product;
import com.example.deloittetest.R;
import com.example.deloittetest.ViewModel.ProductViewModel;

import java.util.ArrayList;
import java.util.List;


public class CartAdapter extends RecyclerView.Adapter {

    public interface IOnDeleteFromCart {
        void onDeleteItemFromCart(int id);
    }

    private final LayoutInflater mInflater;
    List<Product> mProducts = new ArrayList<>();
    List<Cart> mCartList = new ArrayList<>();
    private Context mContext;
    IOnDeleteFromCart mListener;


    public CartAdapter(List<Cart> cart, Context context, IOnDeleteFromCart listener, ProductViewModel prodViewModel) {
        mCartList.clear();
        mCartList = new ArrayList<>(cart);
        updateDataSet(cart, prodViewModel);
        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mListener = listener;
    }

    private void updateDataSet(List<Cart> cartList, ProductViewModel prodViewModel) {
        mCartList.clear();
        mCartList = new ArrayList<>(cartList);
        mProducts.clear();
        for (Cart cart : cartList) {
            mProducts.add(prodViewModel.getProductByID(cart.getProductId()));
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.product_item, parent, false);
        return new DataHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        ((DataHolder) holder).populateData(mContext, mProducts.get(position), position);
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }

    public void updateAdapter(List<Cart> cartList, ProductViewModel prodViewModel) {
        updateDataSet(cartList, prodViewModel);
        notifyDataSetChanged();
    }

    class DataHolder extends RecyclerView.ViewHolder {

        TextView mProductName;
        TextView mOldPrice;
        TextView mNewPrice;
        TextView mCategoryTv;
        Button mAddToCart;
        TextView mAddToWishlist;

        public DataHolder(@NonNull View itemView) {
            super(itemView);
            mOldPrice = itemView.findViewById(R.id.product_old_price);
            mProductName = itemView.findViewById(R.id.product_name);
            mCategoryTv = itemView.findViewById(R.id.tv_category);
            mNewPrice = itemView.findViewById(R.id.product_price);
            mAddToCart = itemView.findViewById(R.id.btn_add_to_cart);
            mAddToWishlist = itemView.findViewById(R.id.add_to_wishlist);
        }


        public void populateData(Context ctx, Product product, int position) {
            mAddToCart.setTag(position);
            mAddToWishlist.setTag(position);
            mProductName.setText(product.getName());
            mNewPrice.setText("$ " + product.getPrice());


            if ("null".equalsIgnoreCase(product.getOldPrice())) {
                mOldPrice.setVisibility(View.GONE);
            } else {
                mOldPrice.setText(product.getOldPrice());
            }


            if ("null".equalsIgnoreCase(product.getCategory())) {
                mCategoryTv.setVisibility(View.GONE);
            } else {
                mCategoryTv.setText(mContext.getString(R.string.category, product.getCategory()));
            }

            // Is cart view open
            mAddToCart.setText(mContext.getString(R.string.delete_item));
            mAddToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int) view.getTag();
                    mListener.onDeleteItemFromCart(mCartList.get(pos).getId());
                }
            });


            mAddToWishlist.setVisibility(View.GONE);
        }
    }
}
