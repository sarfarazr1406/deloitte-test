package com.example.deloittetest.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.deloittetest.Application.Caches.WishlistDataCache;
import com.example.deloittetest.Application.Models.Product;
import com.example.deloittetest.R;

import java.util.ArrayList;
import java.util.List;


public class ProductAdapter extends RecyclerView.Adapter {

    private boolean mIsForWishList;

    public interface IOnAddToCart {
        void onAddToCart(int id);
        void onAddToWishlist(int id);
    }

    private final LayoutInflater mInflater;
    List<Product> mProducts = new ArrayList<>();
    private Context mContext;
    IOnAddToCart mListener;


    public ProductAdapter(List<Product> mProducts, Context context, IOnAddToCart listener) {
        this.mProducts = mProducts;
        mContext = context;
        mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mListener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.product_item, parent, false);
        return new DataHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


        ((DataHolder)holder).populateData(mContext, mProducts.get(position), position, mIsForWishList);
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }

    public void updateAdapter(List<Product> products, boolean isForWishlist, boolean isForCart) {
        mIsForWishList = isForWishlist;
        mProducts.clear();
        mProducts = null;
        mProducts = new ArrayList<>(products);
        notifyDataSetChanged();
    }

    public void updateItem(Product prod) {
        notifyItemChanged(mProducts.indexOf(prod));
    }

    class DataHolder extends RecyclerView.ViewHolder {

        TextView mProductName;
        TextView mOldPrice;
        TextView mNewPrice;
        TextView mCategoryTv;
        Button mAddToCart;
        TextView mAddToWishlist;

        public DataHolder(@NonNull View itemView) {
            super(itemView);
            mOldPrice = itemView.findViewById(R.id.product_old_price);
            mProductName = itemView.findViewById(R.id.product_name);
            mCategoryTv = itemView.findViewById(R.id.tv_category);
            mNewPrice = itemView.findViewById(R.id.product_price);
            mAddToCart = itemView.findViewById(R.id.btn_add_to_cart);
            mAddToWishlist = itemView.findViewById(R.id.add_to_wishlist);
        }


        public void populateData(Context ctx, Product product, int position, boolean isForWishlist) {
            mAddToCart.setTag(position);
            mAddToWishlist.setTag(position);
            mProductName.setText(product.getName());
            mNewPrice.setText("$ "+ product.getPrice());

            if ("null".equalsIgnoreCase(product.getOldPrice())) {
                mOldPrice.setVisibility(View.GONE);
            } else {
                mOldPrice.setText(product.getOldPrice());
            }


            if ("null".equalsIgnoreCase(product.getCategory())) {
                mCategoryTv.setVisibility(View.GONE);
            } else {
                mCategoryTv.setText(mContext.getString(R.string.category, product.getCategory()));
            }

            if (product.getStock() <= 0) {
                // If product is not in stock, disable add to cart button
                mAddToCart.setBackgroundColor(mContext.getResources().getColor(R.color.colorLightRed));
                mAddToCart.setText(mContext.getString(R.string.out_of_stock));
                mAddToCart.setClickable(false);
            } else {
                // If product is in stock, let user add it to cart

                mAddToCart.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
                mAddToCart.setText(mContext.getString(R.string.add_to_cart));
                mAddToCart.setClickable(true);
                mAddToCart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = (int)view.getTag();
                        mListener.onAddToCart(mProducts.get(pos).getId());
                    }
                });
            }

            if (isForWishlist) {
                mAddToWishlist.setVisibility(View.GONE);
            } else if (!WishlistDataCache.getInstance().getProductCache().contains(product.getId()+"")) {
                mAddToWishlist.setVisibility(View.VISIBLE);

                mAddToWishlist.setText(mContext.getString(R.string.add_to_wishlist));
                mAddToWishlist.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
                mAddToWishlist.setClickable(true);
                mAddToWishlist.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = (int)view.getTag();
                        mListener.onAddToWishlist(mProducts.get(pos).getId());
                    }
                });
            } else {
                mAddToWishlist.setVisibility(View.VISIBLE);

                mAddToWishlist.setText(mContext.getString(R.string.wishlisted));
                mAddToWishlist.setBackgroundColor(mContext.getResources().getColor(R.color.colorTransparent));
                mAddToWishlist.setClickable(false);
            }
        }
    }
}
