package com.example.deloittetest.Application.ApplicationEngines.Applications;

import android.content.Context;

import com.example.deloittetest.Application.Models.Product;
import com.example.deloittetest.Utils.InterfaceHandlers;

import java.util.List;

public interface ICartApplication {
    void addToCart(int id);
    List<Product> getAllCartItems();
    void deleteFromCart(int id);

    void setListener (InterfaceHandlers.ICartListener listener);
    void setAppContext(Context context);
}
