package com.example.deloittetest.Application.ApplicationEngines.Applications;

import android.content.Context;

import com.example.deloittetest.Application.Models.Product;
import com.example.deloittetest.Utils.InterfaceHandlers;

import java.util.List;

public interface IProductApplication {
    Product getProductById(int id);
    List<Product> GetAllProducts(Context context);

    void SetListener(InterfaceHandlers.IProductsLoadedListener listener);
}
