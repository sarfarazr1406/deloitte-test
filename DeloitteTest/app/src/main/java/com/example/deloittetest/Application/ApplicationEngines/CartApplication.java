package com.example.deloittetest.Application.ApplicationEngines;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.deloittetest.Application.ApplicationEngines.Applications.ICartApplication;
import com.example.deloittetest.Application.Models.Product;
import com.example.deloittetest.Application.Utils.Constants;
import com.example.deloittetest.Application.Utils.MyRequestQueSingleton;
import com.example.deloittetest.R;
import com.example.deloittetest.Application.Utils.AppUtils;
import com.example.deloittetest.Utils.InterfaceHandlers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Network layer to handle all API calls for cart application
public class CartApplication implements ICartApplication {
    Context mContext;
    InterfaceHandlers.ICartListener mListener;

    public void setListener(InterfaceHandlers.ICartListener listener) {
        mListener = listener;
    }

    public void setAppContext(Context ctx) {
        mContext = ctx;
    }

    @Override
    public void addToCart(int id) {
        final int[] statusCode = new int[1];
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Constants.baseUrl + "cart?productId=" + id, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.i("saz", "hello: " + response.toString());
                try {
                    if (mListener != null) {
                        mListener.OnProductAddedToCart(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("saz", "Error: " + error.toString());
                mListener.OnProductAddedToCart(mContext.getString(R.string.cart_add_error_msg));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put(Constants.apiKey, Constants.apiValue);
                return map;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                Log.i("saz", response.statusCode + "");
                statusCode[0] = response.statusCode;
                return super.parseNetworkResponse(response);
            }
        };

        //jsonObjectRequest = (JsonObjectRequest) AppUtils.setRetryPolicyForRequest(jsonObjectRequest);
        MyRequestQueSingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
    }


    /// Network request to get all the cart items
    @Override
    public List<Product> getAllCartItems() {
        final int[] statusCode = new int[1];
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, Constants.baseUrl + "cart", null, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                Log.i("saz", "hello: " + response.toString());
                if (mListener != null) {
                    mListener.OnCartLoaded(response, statusCode[0]);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("saz", error.toString());
                if (mListener != null) {
                    mListener.OnCartLoaded(null, statusCode[0]);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put(Constants.apiKey, Constants.apiValue);
                return map;
            }

            @Override
            protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                Log.i("saz", response.statusCode + "");
                statusCode[0] = response.statusCode;
                return super.parseNetworkResponse(response);
            }
        };

        jsonArrayRequest = (JsonArrayRequest) AppUtils.setRetryPolicyForRequest(jsonArrayRequest);
        MyRequestQueSingleton.getInstance(mContext).addToRequestQueue(jsonArrayRequest);
        return null;
    }

    @Override
    public void deleteFromCart(int id) {
        final int[] statusCode = new int[1];
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.DELETE, Constants.baseUrl + "cart?id=" + id, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.i("saz", "hello: " + response.toString());
                getAllCartItems();
                if (mListener != null) {
                    mListener.OnCartItemDeleted(statusCode[0]);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("saz", "Error: " + error.toString());
                getAllCartItems();
                mListener.OnCartItemDeleted(statusCode[0]);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put(Constants.apiKey, Constants.apiValue);
                return map;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                Log.i("saz", response.statusCode + "");
                statusCode[0] = response.statusCode;
                return super.parseNetworkResponse(response);
            }
        };

        //jsonObjectRequest = (JsonObjectRequest) AppUtils.setRetryPolicyForRequest(jsonObjectRequest);
        MyRequestQueSingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
    }
}
