package com.example.deloittetest.Application.ApplicationEngines;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.deloittetest.Application.ApplicationEngines.Applications.IProductApplication;
import com.example.deloittetest.Application.Models.Product;
import com.example.deloittetest.Application.Utils.Constants;
import com.example.deloittetest.Application.Utils.MyRequestQueSingleton;
import com.example.deloittetest.Application.Utils.AppUtils;
import com.example.deloittetest.Utils.InterfaceHandlers;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Network layer to handle all API calls for product application

public class ProductApplication implements IProductApplication {

    private InterfaceHandlers.IProductsLoadedListener mListener;

    @Override
    public Product getProductById(int id) {
        return null;
    }

    @Override
    public List<Product> GetAllProducts(Context context) {
        final int[] statusCode = new int[1];
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, Constants.baseUrl + "products", null, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                Log.i("saz","hello: "+response.toString());
                if (mListener != null) {
                    mListener.OnProductsDownloaded(response, statusCode[0]);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("saz",error.toString());
                if (mListener != null) {
                    mListener.OnProductsDownloaded(null, statusCode[0]);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put(Constants.apiKey, Constants.apiValue);
                return map;
            }

            @Override
            protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                Log.i("saz", response.statusCode+"");
                statusCode[0] = response.statusCode;
                return super.parseNetworkResponse(response);
            }
        };

        jsonArrayRequest = (JsonArrayRequest) AppUtils.setRetryPolicyForRequest(jsonArrayRequest);
        MyRequestQueSingleton.getInstance(context).addToRequestQueue(jsonArrayRequest);
        return null;
    }

    @Override
    public void SetListener(InterfaceHandlers.IProductsLoadedListener listener) {
        mListener = listener;
    }
}
