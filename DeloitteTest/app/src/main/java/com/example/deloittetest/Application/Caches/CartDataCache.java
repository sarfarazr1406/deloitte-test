package com.example.deloittetest.Application.Caches;

import com.example.deloittetest.Application.Models.Cart;
import com.example.deloittetest.Application.Models.Product;

import java.util.ArrayList;
import java.util.List;

// Data cache to store the cart products so as to avoid API calls again and again
public class CartDataCache {
    private static CartDataCache cartDataCache;
    public static synchronized CartDataCache getInstance() {

        if (cartDataCache == null) {
            cartDataCache = new CartDataCache();
        }
        return cartDataCache;
    }

    List<Cart> mProductList = new ArrayList<>();

    public void setDataCache(List<Cart> products) {
        mProductList.clear();
        mProductList = null;
        mProductList = new ArrayList<>(products);
    }

    public List<Cart> getProductCache() {
        return mProductList;
    }
}
