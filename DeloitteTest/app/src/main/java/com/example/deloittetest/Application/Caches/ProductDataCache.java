package com.example.deloittetest.Application.Caches;

import android.content.Context;

import com.example.deloittetest.Application.Models.Product;

import java.util.ArrayList;
import java.util.List;

// Data cache to store the products so as to avoid API calls again and again
public final class ProductDataCache {
    private static ProductDataCache productDataCache;

    public static synchronized ProductDataCache getInstance() {
        if (productDataCache == null) {
            productDataCache = new ProductDataCache();
        }
        return productDataCache;
    }

    List<Product> mProductList = new ArrayList<>();

    public void setDataCache(List<Product> products) {
        mProductList = products;
    }

    public List<Product> getProductCache() {
        return mProductList;
    }
}
