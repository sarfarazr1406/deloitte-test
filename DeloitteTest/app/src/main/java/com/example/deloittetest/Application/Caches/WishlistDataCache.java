package com.example.deloittetest.Application.Caches;

import com.example.deloittetest.Application.Models.Product;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class WishlistDataCache {
    private static WishlistDataCache productDataCache;

    public static synchronized WishlistDataCache getInstance() {
        if (productDataCache == null) {
            productDataCache = new WishlistDataCache();
        }
        return productDataCache;
    }

    Set<String> mWishList = new HashSet<>();

    public void setDataCache(Set<String> products) {
        mWishList = products;
    }

    public Set<String> getProductCache() {
        return mWishList;
    }
}
