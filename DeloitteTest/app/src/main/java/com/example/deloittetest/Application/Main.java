package com.example.deloittetest.Application;

import android.content.Context;

import com.example.deloittetest.Application.ApplicationEngines.Applications.ICartApplication;
import com.example.deloittetest.Application.ApplicationEngines.Applications.IProductApplication;
import com.example.deloittetest.Application.ApplicationEngines.CartApplication;
import com.example.deloittetest.Application.ApplicationEngines.ProductApplication;
import com.example.deloittetest.Application.Utils.MyRequestQueSingleton;

// Main class which instantiates all the network layer objects while starting the app

public class Main {
    IProductApplication mProductAplication;
    ICartApplication mCartApplication;
    public static Main instance;

    public static synchronized Main getInstance() {
        if (instance == null) {
            instance = new Main();
        }
        return instance;
    }

    public void startApplications() {
        mProductAplication = new ProductApplication();
        mCartApplication = new CartApplication();
    }

    public ICartApplication getCartApplication() {
        return mCartApplication;
    }

    public IProductApplication getProductApplication() {
        return mProductAplication;
    }
}
