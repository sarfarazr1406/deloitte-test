package com.example.deloittetest.Application.Utils;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.toolbox.JsonRequest;
import com.example.deloittetest.Application.Utils.Constants;

public class AppUtils {

    public static JsonRequest setRetryPolicyForRequest(JsonRequest request) {
        request.setRetryPolicy(new DefaultRetryPolicy(
                Constants.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        return request;
    }
}
