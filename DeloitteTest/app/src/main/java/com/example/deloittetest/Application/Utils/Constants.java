package com.example.deloittetest.Application.Utils;

public class Constants {
    public static final int MY_SOCKET_TIMEOUT_MS = 5000;
    public static final String PRICE_KEY = "priceKey";
    public static final String IS_FOR_WISHLIST = "IsForWoshlist";
    public static String baseUrl = "https://2klqdzs603.execute-api.eu-west-2.amazonaws.com/cloths/";
    public static String apiKey = "X-API-KEY";
    public static String apiValue = "dd378e2802-1ba8-4093-bfe2-4dfc900cc8d7";
    public static int STATUSCODE_OK = 200;
    public static int STATUSCODE_INVALID_KEY = 401;
    public static int STATUSCODE_ADDED_TO_CART = 201;
    public static int STATUSCODE_NOT_IN_STOCK = 403;
    public static int STATUSCODE_NO_PRODUCT = 404;
    public static int STATUSCODE_PRODUCT_DELETED = 404;
}

