package com.example.deloittetest.Fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.deloittetest.Activities.BaseActivity;
import com.example.deloittetest.Adapters.CartAdapter;
import com.example.deloittetest.Adapters.ProductAdapter;
import com.example.deloittetest.Application.Caches.CartDataCache;
import com.example.deloittetest.Application.Caches.WishlistDataCache;
import com.example.deloittetest.Application.Models.Cart;
import com.example.deloittetest.Application.Models.Product;
import com.example.deloittetest.R;
import com.example.deloittetest.Utils.InterfaceHandlers;
import com.example.deloittetest.ViewModel.CartViewModel;
import com.example.deloittetest.ViewModel.ProductViewModel;

import java.util.ArrayList;
import java.util.List;

public class ProductListFragment extends Fragment implements InterfaceHandlers.IProductList, ProductAdapter.IOnAddToCart, InterfaceHandlers.ICartListenerToView,
        BaseActivity.IOnWishlistClickListener, CartAdapter.IOnDeleteFromCart {

    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    ProductAdapter mAdapter;
    CartAdapter mCartAdapter;
    List<Product> mProducts = new ArrayList<>();
    ProductViewModel mProductViewModel;
    TextView mErrorTv;
    ProgressBar mProgressBar;
    private CartViewModel mCartViewModel;
    private BaseActivity.IOnWishlistClickListener mWishListClickListener;

    public static ProductListFragment newInstance() {

        //Bundle args = new Bundle();
        //args.putBoolean(Constants.IS_FOR_WISHLIST, isForWishlist);
        ProductListFragment fragment = new ProductListFragment();
        //fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_list_fragment, container, false);;
        mRecyclerView = view.findViewById(R.id.productList);
        mErrorTv = view.findViewById(R.id.tvErrorMessage);
        mProgressBar = view.findViewById(R.id.progress_bar);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        Product product = new Product();
        product.setName("Shampoo");
        mProducts.add(product);
        mAdapter = new ProductAdapter(mProducts, getActivity(), this);
        mRecyclerView.setAdapter(mAdapter);

        mProductViewModel = ((BaseActivity)getActivity()).getProductViewModel();
        mProductViewModel.setListener(this);
        mProductViewModel.FetchProducts(getActivity());

        mCartViewModel =((BaseActivity) getActivity()).getCartViewModel();
        mCartViewModel.setFragmentListenr(this);

        ((BaseActivity)getActivity()).setWshlistClickListener(this);
    }

    @Override
    public void OnProductsLoaded(List<Product> products, String message) {

        mProgressBar.setVisibility(View.GONE);
        if (mRecyclerView.getAdapter() == mCartAdapter) {
            mRecyclerView.setAdapter(null);
            mRecyclerView.setAdapter(mAdapter);
        }
        if (message.equalsIgnoreCase("OK")) {
            if (products == null || products.size() == 0) {
                mRecyclerView.setVisibility(View.GONE);
                mErrorTv.setVisibility(View.VISIBLE);
                mErrorTv.setText("No products to show!");
            } else {
                mErrorTv.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
                mAdapter.updateAdapter(products, false, false);
            }
        } else {
            Log.i("saz","Error in data retrieval: "+message);
            mRecyclerView.setVisibility(View.GONE);
            mErrorTv.setVisibility(View.VISIBLE);
            mErrorTv.setText(message);
        }

    }

    @Override
    public void onAddToCart(int id) {
        mCartViewModel.addToCart(getActivity(), id);
    }

    @Override
    public void onAddToWishlist(int id) {
        Toast.makeText(getActivity(), "Added to wishlist", Toast.LENGTH_SHORT).show();
        WishlistDataCache.getInstance().getProductCache().add(id+"");

        mAdapter.updateItem(mProductViewModel.getProductByID(id));
    }

    @Override
    public void onDeleteItemFromCart(int id) {
        mCartViewModel.deleteItemFromCart(id);
    }

    @Override
    public void OnProductAdded(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void OnCartLoadedInApplication(List<Cart> cartList) {

    }

    @Override
    public void OnCartItemDeleted(int statusCode) {
        if (mRecyclerView.getAdapter() == mCartAdapter) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.product_deleted), Toast.LENGTH_SHORT).show();
            if (CartDataCache.getInstance().getProductCache().size() == 0) {
                mRecyclerView.setVisibility(View.GONE);
                mErrorTv.setVisibility(View.VISIBLE);
                mErrorTv.setText(R.string.cart_empty);
            } else {
                mCartAdapter.updateAdapter(CartDataCache.getInstance().getProductCache(), mProductViewModel);
            }
        }
    }

    @Override
    public void onWishListClicked(boolean isClicked) {
        if (!isClicked) {
            mProductViewModel.FetchProducts(getActivity());
            mRecyclerView.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mProducts.clear();

            for (String str : WishlistDataCache.getInstance().getProductCache()) {
                try {
                    Product prod = mProductViewModel.getProductByID(Integer.parseInt(str));
                    if (prod != null) {
                        mProducts.add(prod);
                    }
                } catch (NumberFormatException e) {
                    // Do nothing
                }

            }
            if (mProducts == null || mProducts.size() == 0) {
                mErrorTv.setText("Wishlist is empty");
                mErrorTv.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            } else {
                mAdapter.updateAdapter(mProducts, true, false);
            }
        }
    }

    @Override
    public void onCartLayoutClicked() {
        if (CartDataCache.getInstance().getProductCache() == null || CartDataCache.getInstance().getProductCache().size() == 0) {
            mErrorTv.setText(getActivity().getString(R.string.cart_empty));
            mErrorTv.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        } else {
            setCartAdapter();
        }
    }

    private void setCartAdapter() {
        mCartAdapter = null;
        mCartAdapter = new CartAdapter(CartDataCache.getInstance().getProductCache(), getActivity(), this, mProductViewModel);
        mRecyclerView.setAdapter(null);
        mRecyclerView.setAdapter(mCartAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((BaseActivity)getActivity()).setWshlistClickListener(null);
    }

    public void setWshlistClickListener(BaseActivity.IOnWishlistClickListener listener) {
        mWishListClickListener = listener;
    }
}
