package com.example.deloittetest.Utils;

import com.example.deloittetest.Application.Models.Cart;
import com.example.deloittetest.Application.Models.Product;

import org.json.JSONArray;

import java.util.List;

public class InterfaceHandlers {

    // Interface for ViewModel to Views
    public interface IProductList {
        void OnProductsLoaded(List<Product> products, String message);
    }

    public interface ICartListenerToView {

        void OnProductAdded(String message);
        void OnCartLoadedInApplication(List<Cart> cartList);
        void OnCartItemDeleted(int statusCode);
    }

    // Interface for Application to ViewModels
    public interface IProductsLoadedListener {
        void OnProductsDownloaded(JSONArray jsonArray, int statusCode);
    }

    public interface ICartListener {
        void OnProductAddedToCart(String message);
        void OnCartLoaded(JSONArray jsonArray, int statusCode);
        void OnCartItemDeleted(int statusCode);
    }
}
