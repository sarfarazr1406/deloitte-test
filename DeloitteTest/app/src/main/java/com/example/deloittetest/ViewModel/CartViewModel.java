package com.example.deloittetest.ViewModel;

import android.content.Context;
import android.util.Log;

import com.example.deloittetest.Application.ApplicationEngines.Applications.ICartApplication;
import com.example.deloittetest.Application.Caches.CartDataCache;
import com.example.deloittetest.Application.Main;
import com.example.deloittetest.Application.Models.Cart;
import com.example.deloittetest.Utils.InterfaceHandlers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CartViewModel implements InterfaceHandlers.ICartListener {
    private final Context mContext;
    ICartApplication cartApplication;
    private InterfaceHandlers.ICartListenerToView mActivityListener;
    private InterfaceHandlers.ICartListenerToView mProductFragmetListener;
    private List<Cart> mCartItems = new ArrayList<>();

    public CartViewModel(Context context) {
        mContext = context;
        cartApplication = Main.getInstance().getCartApplication();
        cartApplication.setAppContext(context);
        cartApplication.setListener(this);
    }

    public void addToCart(Context context, int id) {
        if (cartApplication != null) {
            cartApplication.addToCart(id);
        }
    }

    @Override
    public void OnProductAddedToCart(String statusCode) {
        mProductFragmetListener.OnProductAdded(statusCode);
        cartApplication.getAllCartItems();
    }

    public void FetchCartItems() {
        cartApplication.getAllCartItems();
    }

    @Override
    public void OnCartLoaded(JSONArray jsonArray, int statusCode) {
        if (jsonArray != null) {
            mCartItems.clear();
            Log.i("saz", jsonArray.toString());
            for (int i = 0; i < jsonArray.length(); i++) {
                Cart cart = new Cart();
                try {
                    JSONObject obj = (JSONObject) jsonArray.get(i);
                    cart.setId((int) obj.get("id"));
                    cart.setProductId((int) obj.get("productId"));
                    mCartItems.add(cart);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            CartDataCache.getInstance().setDataCache(mCartItems);

            mActivityListener.OnCartLoadedInApplication(CartDataCache.getInstance().getProductCache());
            mProductFragmetListener.OnCartItemDeleted(statusCode);
        } else if (statusCode == 401) {

        }
    }

    @Override
    public void OnCartItemDeleted(int statusCode) {
    }

    public void setActivityListener(InterfaceHandlers.ICartListenerToView listener) {
        mActivityListener = listener;
    }

    public void setFragmentListenr(InterfaceHandlers.ICartListenerToView listener) {
        mProductFragmetListener = listener;
    }

    public void deleteItemFromCart(int id) {
        cartApplication.deleteFromCart(id);
    }
}
