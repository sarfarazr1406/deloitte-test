package com.example.deloittetest.ViewModel;

import android.content.Context;
import android.util.Log;

import com.example.deloittetest.Activities.BaseActivity;
import com.example.deloittetest.Application.ApplicationEngines.Applications.IProductApplication;
import com.example.deloittetest.Application.ApplicationEngines.ProductApplication;
import com.example.deloittetest.Application.Caches.ProductDataCache;
import com.example.deloittetest.Application.Main;
import com.example.deloittetest.Application.Models.Product;
import com.example.deloittetest.Application.Utils.Constants;
import com.example.deloittetest.Utils.InterfaceHandlers;
import com.example.deloittetest.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

// ViewModel in the MVVM model which handles the model and performs all the business logic.
// It masks all the overhead operations from the UI layer.
public class ProductViewModel implements InterfaceHandlers.IProductsLoadedListener {
    private static final String PRODUCT_VIEW = "product_view";
    IProductApplication productApplication;
    List<Product> mProducts = new ArrayList<>();
    Context mContext;
    private InterfaceHandlers.IProductList mListener;
    private InterfaceHandlers.IProductList mActivityListener;

    public ProductViewModel() {
        productApplication = Main.getInstance().getProductApplication();
        productApplication.SetListener(this);
    }

    public void FetchProducts(Context ctx) {
        mContext = ctx;
        productApplication.GetAllProducts(ctx);
    }

    @Override
    public void OnProductsDownloaded(JSONArray jsonArray, int statusCode) {
        String msg;
        mProducts.clear();
        if (jsonArray != null) {
            Log.i("saz", jsonArray.toString());
            for (int i = 0; i < jsonArray.length(); i++) {
                Product product = new Product();
                try {
                    JSONObject obj = (JSONObject) jsonArray.get(i);
                    product.setName(obj.get("name").toString());
                    product.setId((int) obj.get("id"));
                    product.setCategory(obj.get("category").toString());
                    product.setPrice(obj.get("price").toString());
                    product.setOldPrice(obj.get("oldPrice").toString());
                    product.setStock((int) obj.get("stock"));
                    Log.i(PRODUCT_VIEW, "Product received at ViewModel: id- "+product.getId()+", Name- "+product.getName());
                    mProducts.add(product);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            ProductDataCache.getInstance().setDataCache(mProducts);
        }

        if (mListener != null) {
            if (statusCode == Constants.STATUSCODE_OK) {
                    msg = "OK";
                    if (mActivityListener != null) {
                        mActivityListener.OnProductsLoaded(null,"");
                    }
            } else {
                msg = mContext.getString(R.string.msg_error_in_retrievaing);
            }
            mListener.OnProductsLoaded(mProducts, msg);
        }
    }

    public void setListener(InterfaceHandlers.IProductList listener) {
        mListener = listener;
    }

    // Retrieving product from the data cache
    public Product getProductByID(int id) {
        Log.i("saz","Product list: "+ProductDataCache.getInstance().getProductCache());
        for (Product product : ProductDataCache.getInstance().getProductCache()) {
            if (product.getId() == id) {
                return product;
            }
        }
        return null;
    }

    public void setActivityListener(InterfaceHandlers.IProductList listener) {
        mActivityListener = listener;
    }
}
