# README #

This readme defines the project structure of the android app for shopping products

Project structure
Application Layer (Doing all the network operations) - Used Volley library for API requests.
ViewModel layer (Performing all the business logic and offloading UI layer with heavy tasks)
UI layer (Activities and fragments)

Click on the right star icon for accessing wishlist.
